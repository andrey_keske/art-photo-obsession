# Art Photo Obsession
[http://andreykeske.com/projects/artphotoobsession/html/index.html](http://andreykeske.com/projects/artphotoobsession/html/index.html)


## Build and start server

To build use: `grunt build`

To start server use: `grunt serve`


## What I use on this project

[Yeoman](http://yeoman.io/) — project scafollding;

[Bower](http://bower.io/) — package manager;

[GruntJS](http://gruntjs.com/) — task runner;


### Grunt tasks:

[Assemble](https://github.com/assemble/assemble) — static site generator;

[SASS](https://github.com/gruntjs/grunt-contrib-sass) — compile Sass to CSS;

[autoprefixer](https://github.com/postcss/autoprefixer) — Parse CSS and add vendor prefixes to rules;

[Imagemin](https://github.com/gruntjs/grunt-contrib-imagemin) — minify PNG and JPEG images;

[HTMLMin](https://github.com/gruntjs/grunt-contrib-htmlmin) — minify HTML;

[JSHint](https://github.com/gruntjs/grunt-contrib-jshint) — validate js files;

[Uglify](https://github.com/nDmitry/grunt-autoprefixer) — minify files;

[Svgmin](https://www.npmjs.org/package/grunt-svgmin) — minify SVG

[Grunt Watch](https://github.com/gruntjs/grunt-contrib-watch) — run tasks whenever watched files change;

[Connect](https://github.com/gruntjs/grunt-contrib-connect) — start a static web server;

[Clean](https://github.com/gruntjs/grunt-contrib-clean) — clear files and folders;

[Copy](https://github.com/gruntjs/grunt-contrib-copy) — copy files and folders;

[Concurrent](https://github.com/sindresorhus/grunt-concurrent) — run grunt tasks concurrently;

[Modernizr](https://github.com/Modernizr/grunt-modernizr)

[Time Grunt](https://github.com/sindresorhus/time-grunt)

[Load Grunt Tasks](https://github.com/sindresorhus/load-grunt-tasks)

