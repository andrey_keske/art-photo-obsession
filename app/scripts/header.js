!(function() {

  var header = {

    showSearchBox: function() {
      'use strict';

      $('.phone-search-box').addClass('show');
    },

    hideSearchBox: function() {
      'use strict';

      $('.phone-search-box').removeClass('show');
    },

    init: (function() {
      'use strict';

      $(document).ready(function() {
        $('.js-show-searchbox').on('click', function() {
          header.showSearchBox();
        });
        $('.js-close-searchbox').on('click', function() {
          header.hideSearchBox();
        });
      });
    }())

  }

}());
