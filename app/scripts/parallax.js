/**
 * Parallax
 */

!(function () {

    var CLASS = '.parallax',
	    TEXT_CLASS = '.parallax-text',

	    IMAGE_SPEED = 0.2,
	    TEXT_SPEED = 0.7,

        MIN_SCREEN_SIZE = 980;

    var parallax = {

        setImage: function (elem, src) {
            "use strict";

            elem
                .css('background', 'url(' + src + ')')
                .css('background-size', 'cover');
        },


        on: function () {
            "use strict";

            var scrollTop = $(window).scrollTop(),
                parallaxElem = $(CLASS),
                textBlock = '';

            parallaxElem.each(function () {
                $(this).css('background-position', '0px ' + (scrollTop * IMAGE_SPEED) + 'px');

                textBlock = $(this).find('.parallax-text');
                textBlock.css('margin-top', scrollTop * -1 * TEXT_SPEED);
            });
        },

        init: (function () {
            "use strict";

            $(window).scroll(function () {
                if ($(window).width() > MIN_SCREEN_SIZE) {
                    parallax.on();
                }
            });
        }())

    };

}());