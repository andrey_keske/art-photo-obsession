/**
 * Touch Slider
 */
 
!(function() {

    // Slides arrows elements
    var $PREV = '.slidesjs-previous',
        $NEXT = '.slidesjs-next';

    // Animate speed (ms)
    var SPEED = 5000;

    var touchSlider = {

        /**
         * Render
         *
         * Reposition arrows
         */
        render: function() {
            "use strict";

            var sliderHeight = 0;

            $('#slides').each(function() {
                sliderHeight = $(this).height();

                $(this).find($PREV).css('margin-top', (sliderHeight * -1) + (sliderHeight / 2) - ($(this).find($PREV).height() / 2));
                $(this).find($NEXT).css('margin-top', (sliderHeight * -1) + (sliderHeight / 2) - ($(this).find($NEXT).height() / 2));
            });
        },

        /**
         * Animate
         *
         * Auto change slide every `SPEED` ms.
         */
        animateSlides: function() {
            "use strict";

            $('#slides').each(function() {
                if ($(this).hasClass('js-animate-slides')) {
                    touchSlider.changeSlideAuto($(this));
                }
            });
        },

        /**
         * Change slide
         *
         * @param $elem Object
         */
        changeSlideAuto: function($elem) {
            "use strict";

            setTimeout(function() {
                $elem.find($NEXT).click();
                touchSlider.changeSlideAuto($elem);
            }, SPEED);
        },

        init: (function() {
            "use strict";

            $(document).ready(function() {
                /**
                 * Home page
                 */
                $('.in-home-page').slidesjs({
                    width: 940,
                    height: 470
                });
            });

            $(window).load(function() {
                touchSlider.render();

                touchSlider.animateSlides();
            });

            $(window).resize(function() {
                touchSlider.render();
            });
        }())
    };

});